import 'package:flutter/material.dart';
import 'package:petloka_app/Page/ExamplePage/example.dart';


const APP_TITLE = "Example Page";


void main() => runApp(new MyApp());



class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
        title: APP_TITLE,
      theme: ThemeData(
          primarySwatch: Colors.blue
      ),
        home: LoginPage()
    );
  }
}

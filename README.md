# Petloka App

## Rules


- Never push code to develop directly, push to feature/*current_work branch instead



- Make a Pull Request to develop branch and assign it to yourself 

but if you contribute/write something to a files that previously written by someone else, assign that person too

- Make new branch under Develop branch, and name it to feature/*current_work -> this makes a branch under feature directory

- NEVER WORK UNDER DEVELOP BRANCH!!!!!!!! work under feature/*current_work instead.

- Develop Branch is compilation of feature/*work_folders
- Develop Branch is current state of our Application

- Make a hotfix/*current_bug_fix under master branch or develop branch if something critical/crucial happened



```
note: current_work can be anything, depends on what you working on
example -> feature/layout
```


## Folder and File Structure

- Create seperate folder for every views under lib/Page/*view_folder

example: for Login page, put it to lib/Page/Login/login.dart

- Create seperate file for every feature, and a import container file

example: if you want to make Login feature 

make login_form.dart, login_auth.dart, login_page.dart and login.dart for import container

lib/Page/Login/login.dart <- this file only export the 3 files below
```
inside login.dart

export 'login_auth.dart';
export 'login_page.dart';
export 'login_form.dart';

```
lib/Page/Login/login_auth.dart

lib/Page/Login/login_page.dart

lib/Page/Login/login_form.dart

if you want to use the Login feature, you can just import login.dart in any page you desire


## Install Flutter
https://flutter.io/docs/get-started/install


## Clone Project

clone project first

```
in terminal

git clone https://gitlab.com/wearinasia/petloka-flutter.git


command above gonna make you a directory named petloka-flutter
```
checkout your working branch

example:
```
in terminal

cd petloka-flutter

git checkout feature/layout

this means you gonna code everything under feature/layout branch

-------------------------------------------------------------------------
for availables branches type:
git branch




```

install dependencies -> if your IDE cannot recognize the dependencies

```
in terminal
cd petloka-flutter

flutter packages get
```


# Issues

- Please report issues(installation etc), bug and error in gitlab or trello so @gabeaventh can assist you through the installation process
